import 'dart:convert';
import 'dart:io';

import 'package:flutter_interview/models/user.dart';
import 'package:http/http.dart' as http;

class AuthApi {
  static Future<User> login(String username, String password) async {
    try {
      await http.post(Uri.parse("https://hookb.in/mZZ8pmBdk6ilzXNNzQGp"),
          headers: {"Content-Type": "application/json"},
          body: JsonEncoder()
              .convert({"username": username, "password": password}));
      return User(email: "abumuhab98@gmail.com", username: "abumuhab");
    } on IOException catch (e) {
      throw "No internet connection";
    }
  }

  static Future<User> signup(
      String username, String email, String password) async {
    try {
      await http.post(Uri.parse("https://hookb.in/mZZ8pmBdk6ilzXNNzQGp"),
          headers: {"Content-Type": "application/json"},
          body: JsonEncoder().convert(
              {"username": username, "password": password, "email": email}));

      return User(email: "abumuhab98@gmail.com", username: "abumuhab");
    } on IOException catch (e) {
      throw "No internet connection";
    }
  }
}
