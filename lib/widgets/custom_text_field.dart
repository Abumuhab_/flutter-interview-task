import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController? controller;
  final String label;
  final bool obscureText;
  final double? width;
  final double? height;
  final String? Function(String?)? validator;
  CustomTextField(
      {this.controller,
      this.label = "",
      this.obscureText = false,
      this.width,
      this.height,
      this.validator});

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  late bool obscureText;

  @override
  void initState() {
    obscureText = widget.obscureText;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14), color: Color(0xffF0F0F0)),
      child: Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8, left: 17, right: 17),
        child: Row(
          children: [
            Expanded(
                child: TextFormField(
              validator: widget.validator,
              obscureText: obscureText,
              controller: widget.controller,
              style: TextStyle(fontSize: 17),
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.zero,
                  border: InputBorder.none,
                  labelText: widget.label,
                  labelStyle: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                      color: Color(0xffAEAEB2))),
            )),
            widget.obscureText
                ? Row(
                    children: [
                      SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            obscureText = !obscureText;
                          });
                        },
                        child: Icon(Icons.remove_red_eye_outlined),
                      )
                    ],
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
