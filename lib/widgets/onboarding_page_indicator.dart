import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OnboardingProgressStep extends StatefulWidget {
  final double radius;
  final int steps;
  final int initialIndex;
  OnboardingProgressStep(
      {this.radius = 20, this.initialIndex = 1, this.steps = 1});
  @override
  State createState() => OnboardingProgressStepState();
}

class OnboardingProgressStepState extends State<OnboardingProgressStep> {
  late int currentIndex;

  @override
  void initState() {
    currentIndex = widget.initialIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> chips = [];
    for (int x = 1; x <= widget.steps; x++) {
      chips.add(Step(
        index: x,
        isCurrentStep: x <= currentIndex,
        radius: widget.radius,
      ));
    }
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [...chips],
      ),
    );
  }
}

class Step extends StatelessWidget {
  final int index;
  final bool isCurrentStep;
  final double radius;
  final Color selectedColor;
  final Color unselectedColor;
  Step(
      {this.index = 1,
      this.isCurrentStep = false,
      this.radius = 20,
      this.selectedColor = Colors.white,
      this.unselectedColor = const Color(0xff404040)});
  @override
  Widget build(BuildContext context) {
    Widget chip = CircleAvatar(
      radius: 20,
      backgroundColor: isCurrentStep ? selectedColor : unselectedColor,
      child: FittedBox(
        child: Padding(
          padding: EdgeInsets.all(radius / 1.9),
          child: Text(
            index.toString(),
            style: TextStyle(
                color: isCurrentStep ? Colors.black : Colors.white,
                fontSize: 13,
                fontWeight: FontWeight.w500),
          ),
        ),
      ),
    );

    if (index == 1) {
      return chip;
    }

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          width: radius,
          child: Padding(
            padding: EdgeInsets.only(left: 5, right: 5),
            child: Divider(
              thickness: radius / 10,
              color: isCurrentStep ? selectedColor : unselectedColor,
            ),
          ),
        ),
        chip,
      ],
    );
  }
}
