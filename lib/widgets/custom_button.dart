import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback? onTap;
  final String? text;
  final double? width;
  final double? height;
  final TextStyle? textStyle;
  CustomButton(
      {this.onTap, this.text = "", this.height, this.width, this.textStyle});
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xffFB724C), Color(0xffFE904B)]),
              borderRadius: BorderRadius.circular(14)),
          child: Center(
            child: Text(
              text!,
              style: textStyle ??
                  TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 17,
                      color: Colors.white),
            ),
          ),
        ));
  }
}
