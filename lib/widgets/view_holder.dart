import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_interview/views/chats.dart';
import 'package:flutter_interview/views/home.dart';
import 'package:flutter_interview/widgets/view_menu.dart';

class ViewHolder extends StatefulWidget {
  @override
  _ViewHolderState createState() => _ViewHolderState();
}

class _ViewHolderState extends State<ViewHolder> {
  late View selectedView;

  @override
  void initState() {
    selectedView = View.HOME;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            Expanded(
                child: selectedView == View.HOME
                    ? Home()
                    : selectedView == View.CHAT
                        ? Chats()
                        : Container(
                            color: Colors.white,
                            child: Center(
                              child: Text(
                                "Screen not implemented",
                                style: TextStyle(color: Colors.grey),
                              ),
                            ),
                          )),
            Container(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.only(left: 22, right: 22, bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ViewMenu(
                      text: "Home",
                      icon: Icons.home_filled,
                      selected: selectedView == View.HOME,
                      onTap: () {
                        if (selectedView != View.HOME) {
                          setState(() {
                            selectedView = View.HOME;
                          });
                        }
                      },
                    ),
                    ViewMenu(
                      text: "Moments",
                      icon: Icons.group_rounded,
                      selected: selectedView == View.MOMENTS,
                      onTap: () {
                        if (selectedView != View.MOMENTS) {
                          setState(() {
                            selectedView = View.MOMENTS;
                          });
                        }
                      },
                    ),
                    ViewMenu(
                      text: "Chat",
                      icon: Icons.send,
                      selected: selectedView == View.CHAT,
                      onTap: () {
                        if (selectedView != View.CHAT) {
                          setState(() {
                            selectedView = View.CHAT;
                          });
                        }
                      },
                    ),
                    ViewMenu(
                      text: "Profile",
                      icon: Icons.account_circle_rounded,
                      selected: selectedView == View.PROFILE,
                      onTap: () {
                        if (selectedView != View.PROFILE) {
                          setState(() {
                            selectedView = View.PROFILE;
                          });
                        }
                      },
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

enum View { HOME, MOMENTS, CHAT, PROFILE }
