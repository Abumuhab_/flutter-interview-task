import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_interview/views/walker_details.dart';

class WalkerCard extends StatelessWidget {
  final int price;
  final int distance;
  final String imageAsset;
  final String walkerName;

  WalkerCard(
      {this.price = 0,
      this.imageAsset = "",
      this.walkerName = "",
      this.distance = 0});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/walker_details",
            arguments:
                WalkerDetailsArgs(imageAsset: imageAsset, name: walkerName));
      },
      child: SizedBox(
        width: 179,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: 179,
              height: 125,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(14),
                child: Image.asset(imageAsset),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(walkerName,
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 17)),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.location_on_outlined,
                          size: 13,
                          color: Colors.grey,
                        ),
                        Text(
                          "$distance km from you",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 10,
                              color: Colors.grey),
                        )
                      ],
                    )
                  ],
                ),
                Container(
                  height: 25,
                  width: 45,
                  decoration: BoxDecoration(
                      color: Color(0xff2B2B2B),
                      borderRadius: BorderRadius.circular(7)),
                  child: Center(
                    child: Text(
                      "\$$price/hr",
                      style: TextStyle(fontSize: 10, color: Colors.white),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
