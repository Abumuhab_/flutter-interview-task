import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_interview/providers/auth.dart';
import 'package:flutter_interview/views/onboarding.dart';
import 'package:flutter_interview/views/signin.dart';
import 'package:flutter_interview/views/signup.dart';
import 'package:flutter_interview/views/walker_details.dart';
import 'package:flutter_interview/widgets/view_holder.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => AuthProvider()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      routes: {
        "/onboarding": (context) => Onboarding(),
        "/signup": (context) => Signup(),
        "/signin": (context) => Signin(),
        "/home": (context) => Consumer<AuthProvider>(
              builder: (context, provider, _) {
                if (provider.authState == AuthState.LOGGED_OUT) {
                  return Onboarding();
                }
                return ViewHolder();
              },
            ),
        "/walker_details": (context) => WalkerDetails()
      },
      initialRoute: "/home",
    );
  }
}
