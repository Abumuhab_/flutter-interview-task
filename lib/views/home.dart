import 'package:flutter/material.dart';
import 'package:flutter_interview/widgets/custom_button.dart';
import 'package:flutter_interview/widgets/walker_card.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 20, right: 20, top: 20),
          color: Colors.white,
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Home",
                            style: TextStyle(
                                fontSize: 34, fontWeight: FontWeight.w700),
                          ),
                          Text(
                            "Explore dog walkers",
                            style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff7A7A7A)),
                          ),
                        ],
                      ),
                      CustomButton(
                        height: 41,
                        width: 104,
                        text: "Book a walk",
                        textStyle: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 10,
                            color: Colors.white),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 21,
                  ),
                  Container(
                    height: 50,
                    padding: EdgeInsets.only(
                        left: 15, right: 15, top: 10, bottom: 10),
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Color(0xffF0F0F0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(Icons.location_on_outlined),
                            Text("Abuja, Nigeria")
                          ],
                        ),
                        Image.asset("images/options.png")
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 23,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Near you",
                        style: TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 34),
                      ),
                      Text("view all",
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              decoration: TextDecoration.underline))
                    ],
                  ),
                  SizedBox(
                    height: 9.5,
                  ),
                  SizedBox(
                    height: 200,
                    child: ListView(
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      children: [
                        WalkerCard(
                          imageAsset: "images/d1.jpg",
                          walkerName: "Mason York",
                          distance: 7,
                          price: 5,
                        ),
                        SizedBox(
                          width: 43,
                        ),
                        WalkerCard(
                          imageAsset: "images/d2.jpg",
                          walkerName: "Mark Greene",
                          distance: 10,
                          price: 8,
                        ),
                        SizedBox(
                          width: 43,
                        ),
                        WalkerCard(
                          imageAsset: "images/d3.jpg",
                          walkerName: "Alan Walker",
                          distance: 11,
                          price: 18,
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    thickness: 2,
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Suggested",
                        style: TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 34),
                      ),
                      Text("view all",
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              decoration: TextDecoration.underline))
                    ],
                  ),
                  SizedBox(
                    height: 9.5,
                  ),
                  SizedBox(
                    height: 200,
                    child: ListView(
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      children: [
                        WalkerCard(
                          imageAsset: "images/d3.jpg",
                          walkerName: "Mason York",
                          distance: 7,
                          price: 5,
                        ),
                        SizedBox(
                          width: 43,
                        ),
                        WalkerCard(
                          imageAsset: "images/d1.jpg",
                          walkerName: "Mark Greene",
                          distance: 10,
                          price: 8,
                        ),
                        SizedBox(
                          width: 43,
                        ),
                        WalkerCard(
                          imageAsset: "images/d2.jpg",
                          walkerName: "Alan Walker",
                          distance: 11,
                          price: 18,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
