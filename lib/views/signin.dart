import 'package:flutter/material.dart';
import 'package:flutter_interview/providers/auth.dart';
import 'package:flutter_interview/util/util.dart';
import 'package:flutter_interview/widgets/custom_button.dart';
import 'package:flutter_interview/widgets/custom_text_field.dart';
import 'package:provider/provider.dart';

class Signin extends StatelessWidget {
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 20, right: 20, top: 20),
          color: Colors.white,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: SafeArea(
              child: Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.arrow_back),
                    ),
                    SizedBox(
                      height: 28,
                    ),
                    Text(
                      "Welcome back",
                      style:
                          TextStyle(fontSize: 34, fontWeight: FontWeight.w700),
                    ),
                    Text(
                      "Fill in details to continue",
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w500,
                          color: Color(0xff7A7A7A)),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextField(
                      width: double.infinity,
                      label: "Email",
                      validator: (val) {
                        if (!validateEmail(val!)) {
                          return "Enter a valid email";
                        }
                      },
                      controller: email,
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextField(
                      width: double.infinity,
                      label: "Password",
                      validator: (val) {
                        if (val!.length < 4) {
                          return "password too short";
                        }
                      },
                      controller: password,
                      obscureText: true,
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomButton(
                      width: double.infinity,
                      height: 58,
                      text: "Sign in",
                      onTap: () async {
                        if (formKey.currentState!.validate()) {
                          showPersistentLoadingIndicator(context);
                          Provider.of<AuthProvider>(context, listen: false)
                              .login(email.text.trim(), password.text.trim())
                              .then((value) {
                            Navigator.popUntil(
                                context, ModalRoute.withName('/home'));
                          }).catchError((err) {
                            Navigator.pop(context);
                            showBasicMessageDialog(err.toString(), context);
                          });
                        }
                      },
                    ),
                    SizedBox(
                      height: 320,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: 220,
                        child: Text.rich(
                          TextSpan(
                              text: "By signing in, i agree with ",
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 13,
                                  color: Colors.grey),
                              children: [
                                TextSpan(
                                  text: "Terms of use ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13,
                                      color: Colors.black),
                                ),
                                TextSpan(
                                  text: "and",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13,
                                      color: Colors.grey),
                                ),
                                TextSpan(
                                  text: " Privacy Policy",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13,
                                      color: Colors.black),
                                )
                              ]),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
