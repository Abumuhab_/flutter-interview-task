import 'package:flutter/material.dart';
import 'package:flutter_interview/widgets/custom_button.dart';

class WalkerDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WalkerDetailsArgs args =
        ModalRoute.of(context)?.settings.arguments as WalkerDetailsArgs;
    return Scaffold(
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.6,
                  width: double.infinity,
                  child: Stack(
                    children: [
                      SizedBox(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height * 0.6,
                        child: Image.asset(
                          args.imageAsset,
                          fit: BoxFit.cover,
                        ),
                      ),
                      SafeArea(
                          child: Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 44,
                                width: 44,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(22),
                                    color: Color.fromARGB(150, 196, 196, 196)),
                                child: Center(
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: Icon(
                                      Icons.close,
                                      color: Color(0xffF7F7F8),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: 44,
                                width: 101,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(22),
                                    color: Color.fromARGB(150, 196, 196, 196)),
                                child: Center(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        "verified",
                                        style: TextStyle(
                                            color: Color(0xffF7F7F8),
                                            fontSize: 13,
                                            fontWeight: FontWeight.w700),
                                      ),
                                      Icon(
                                        Icons.close,
                                        color: Color(0xffF7F7F8),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ))
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.4,
                  child: Container(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding:
                  EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 30),
              // height: MediaQuery.of(context).size.height * 0.55,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(24),
                      topLeft: Radius.circular(24))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    args.name,
                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: 28),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text.rich(TextSpan(
                      text: "5\$",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 13),
                      children: [
                        TextSpan(
                            text: "/hr | ",
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w500,
                                fontSize: 13)),
                        TextSpan(
                            text: "10",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 13)),
                        TextSpan(
                            text: "km |  ",
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w500,
                                fontSize: 13)),
                        TextSpan(
                            text: "4.4  ",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 13)),
                        TextSpan(
                            text: "|  ",
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w500,
                                fontSize: 13)),
                        TextSpan(
                            text: "450 ",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 13)),
                        TextSpan(
                            text: "walks",
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w500,
                                fontSize: 13)),
                      ])),
                  SizedBox(
                    height: 25,
                  ),
                  Divider(),
                  SizedBox(
                    height: 22,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 54,
                        width: 109,
                        decoration: BoxDecoration(
                            color: Color(0xff2B2B2B),
                            borderRadius: BorderRadius.circular(14)),
                        child: Center(
                          child: Text(
                            "About",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 13),
                          ),
                        ),
                      ),
                      Container(
                        height: 54,
                        width: 109,
                        decoration: BoxDecoration(
                            color: Color(0xffF5F5F5),
                            borderRadius: BorderRadius.circular(14)),
                        child: Center(
                          child: Text(
                            "Location",
                            style: TextStyle(
                                color: Color(0xffB0B0B0),
                                fontWeight: FontWeight.w700,
                                fontSize: 13),
                          ),
                        ),
                      ),
                      Container(
                        height: 54,
                        width: 109,
                        decoration: BoxDecoration(
                            color: Color(0xffF5F5F5),
                            borderRadius: BorderRadius.circular(14)),
                        child: Center(
                          child: Text(
                            "Reviews",
                            style: TextStyle(
                                color: Color(0xffB0B0B0),
                                fontWeight: FontWeight.w700,
                                fontSize: 13),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Age",
                            style: TextStyle(
                                color: Color(0xffB0B0B0),
                                fontSize: 13,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text("30 years",
                              style: TextStyle(
                                  color: Color(0xff2B2B2B),
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500))
                        ],
                      ),
                      SizedBox(
                        width: 45,
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Experience",
                            style: TextStyle(
                                color: Color(0xffB0B0B0),
                                fontSize: 13,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            width: 6,
                          ),
                          Text("11 months",
                              style: TextStyle(
                                  color: Color(0xff2B2B2B),
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500))
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: SizedBox(
                      width: 300,
                      child: Text(
                          "Alex has loved dogs since childhood. He is currently a veterinary student. Visits the dog shelter we...",
                          style: TextStyle(
                              color: Color(0xffB0B0B0),
                              fontSize: 13,
                              fontWeight: FontWeight.w500)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: SizedBox(
                      width: 300,
                      child: Text("Read more",
                          style: TextStyle(
                              color: Color(0xffFB724C),
                              fontSize: 13,
                              fontWeight: FontWeight.w500)),
                    ),
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  CustomButton(
                    height: 56,
                    text: "Check Schedule",
                  ),
                  SizedBox(
                    height: 37,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class WalkerDetailsArgs {
  String imageAsset;
  String name;

  WalkerDetailsArgs({this.imageAsset = "", this.name = ""});
}
