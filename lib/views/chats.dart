import 'package:flutter/material.dart';
import 'package:flutter_interview/widgets/chat_preview.dart';
import 'package:flutter_interview/widgets/custom_button.dart';

class Chats extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 20, right: 20, top: 20),
          color: Colors.white,
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Chat",
                  style: TextStyle(fontSize: 34, fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: 21,
                ),
                Container(
                  height: 50,
                  padding:
                      EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Color(0xffF0F0F0),
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [Icon(Icons.search), Text("Search...")],
                      ),
                      Image.asset("images/options.png")
                    ],
                  ),
                ),
                SizedBox(
                  height: 26,
                ),
                Expanded(
                    child: ListView(
                  children: [
                    Divider(),
                    ChatPreview(
                      imageAsset: "images/d1.jpg",
                      name: "Christopher Nolan",
                      message: "How is your dog feeling today?",
                    ),
                    ChatPreview(
                      imageAsset: "images/d2.jpg",
                      name: "Agatha cristie",
                      message: "Puppy doing great this morning!",
                    ),
                    ChatPreview(
                      imageAsset: "images/d3.jpg",
                      name: "Sirena Paul",
                      message: "Dont forget to feed the dog",
                    ),
                  ],
                ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
