import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_interview/widgets/custom_button.dart';
import 'package:flutter_interview/widgets/onboarding_page_indicator.dart';

class Onboarding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Expanded(
                    child: SizedBox(
                      width:  MediaQuery.of(context).size.width,
                      child: Image.asset(
                        "images/onboarding.png",
                        fit: BoxFit.cover,
                      ),
                    )),
                Container(
                  height: 232,
                  color: Color(0xff202020),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: EdgeInsets.only(top: 40, left: 16),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset("images/paw.png"),
                  Image.asset("images/woo.png"),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(left: 21, right: 21, bottom: 57),
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    OnboardingProgressStep(
                      radius: 30,
                      initialIndex: 1,
                      steps: 3,
                    ),
                    SizedBox(height: 40),
                    SizedBox(
                      width: 332,
                      child: Text(
                        "Too tired to walk your dog? Let's help you",
                        style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 22),
                    CustomButton(
                      width: 324,
                      height: 58,
                      text: "Join our community",
                      onTap: () {
                        Navigator.pushNamed(context, "/signup");
                      },
                    ),
                    SizedBox(height: 22),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "Already a member? ",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, "/signin");
                          },
                          child: Text(
                            "Sign in",
                            style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                                color: Color(0xffFB724C)),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
