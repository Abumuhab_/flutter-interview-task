import 'package:flutter/material.dart';
import 'package:flutter_interview/api/auth.dart';
import 'package:flutter_interview/models/user.dart';

class AuthProvider extends ChangeNotifier {
  late AuthState authState;
  User? user;

  AuthProvider() {
    authState = AuthState.LOGGED_OUT;
  }

  Future<void> login(String username, String password) async {
    this.user = await AuthApi.login(username, password);
    if (user != null) {
      authState = AuthState.LOGGED_IN;
    }
    notifyListeners();
  }

  Future<void> signup(String username, String email, String password) async {
    this.user = await AuthApi.signup(username, email, password);
    if (user != null) {
      authState = AuthState.LOGGED_IN;
    }
    notifyListeners();
  }
}

enum AuthState { LOGGED_IN, LOGGED_OUT }
